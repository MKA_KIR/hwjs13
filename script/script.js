let body = document.body;
let link = document.createElement('link');
link.rel = 'stylesheet';

if (localStorage.getItem('Style') === 'newStyle') {
    link.href = 'style/new_style.css';
} else {
    link.href = 'style/style.css';
}
body.appendChild(link);

document.getElementById('btnChange').addEventListener('click', function () {
    if (localStorage.getItem('Style') === 'oldStyle') {
        link.href = 'style/new_style.css';
        localStorage.setItem('Style', 'newStyle');
    } else {
        link.href = 'style/style.css';
        localStorage.setItem('Style', 'oldStyle');
    }
});
